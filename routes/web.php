<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/admin', 'Auth\LoginController@showLoginForm')->name('admin');
Route::get('/admin/download/', 'DownloadController@index')->name('admin.download.index');
Route::get('/admin/download/reload', 'DownloadController@reload')->name('admin.download.reload');

Route::redirect('/home', '/', 301);
// Download
Route::get('/download/{alias}', 'DownloadController@download')->name('download');
//All url served by PagesController
Route::get('/{page?}', 'PagesController@index')->name('pages');

//forms
Route::group(['prefix' => 'mail', 'as' => 'mail.', 'namespace' => 'Mail'], function() {
    Route::post('/subscribe', 'SubscribeController@index')->name('subscribe');
    Route::post('/membership', 'MembershipController@index')->name('membership');
});

// Admin pages
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function() {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('/pages', 'PagesController');
    Route::resource('/settings', 'SettingsController');
});