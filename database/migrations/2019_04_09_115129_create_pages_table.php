<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->string('alias');
	        $table->json('parent_id')->nullable();
	        $table->string('title')->nullable();
	        $table->mediumtext('description')->nullable();
	        $table->unsignedBigInteger('description_clone')->default(0);
	        $table->string('meta_title')->nullable();
	        $table->string('meta_description')->nullable();
	        $table->string('meta_keyword')->nullable();
	        $table->json('form_data')->nullable();
	        $table->tinyInteger('status')->default(0);
	        $table->tinyInteger('top_menu')->default(0);
	        $table->tinyInteger('bottom_menu')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
