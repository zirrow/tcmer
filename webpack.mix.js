const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/images', 'public/images')
    .copyDirectory('resources/fonts', 'public/fonts')
    .copyDirectory('node_modules/font-awesome/fonts', 'public/fonts')
    .styles(
        [
            'resources/css/bootstrap.min.css',
            'node_modules/font-awesome/css/font-awesome.min.css',
        ],
        'public/css/libs.min.css'
    )
    .styles(
        [
            'resources/css/style.min.css',
            'resources/css/custom.css'
        ],
        'public/css/style.min.css'
    )
    .styles(
        [
            'resources/css/admin.css'
        ],
        'public/css/admin.min.css'
    )
    .copyDirectory('resources/js/summernote', 'public/js/summernote')
    .js('resources/js/common.js', 'public/js/common.min.js')
    .js('resources/js/app.js', 'public/js/app.min.js');
