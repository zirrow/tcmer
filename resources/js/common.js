(function ($) {
    $(document).ready(function () {
        //home open list
        $('.home-about__list h3').on('click', function () {
            $(this).closest('li').toggleClass('active').find('p').slideToggle();
        });

        //menu open
        $('.menu-open').on('click', function () {
            $('.menu-mobile').addClass('is-active');
            $('.bg').fadeIn();
        });

        //menu close
        $('.menu-close, .bg').on('click', function () {
            $('.menu-mobile').removeClass('is-active');
            $('.bg').fadeOut();
        });

        //tabs
        $('ul.publications__tabs__caption').on('click', 'li:not(.active)', function () {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.publications__tabs').find('div.publications__tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });

        //publications__card__include
        $('.publications__card__include a').on('click', function (event) {
            event.preventDefault();
            let btnCard = $(this);
            btnCard.toggleClass('active').next('ul').slideToggle();
            if (btnCard.hasClass('active')) {
                btnCard.closest('.publications__card').toggleClass('active');
            } else {
                setTimeout(function () {
                    btnCard.closest('.publications__card').toggleClass('active');
                }, 300);
            }
        });
    });

    //fixed header scroll
    if ($('.header-home').length) {
        var w = $(window),
            customScroll = function(){
                $('.header').toggleClass('header-home', w.scrollTop() <= 100);
            },
            customScrollCheck = function(){
                if(w.width() < 767){
                    customScroll();
                    w.on('scroll', customScroll);
                } else {
                    w.off('scroll', customScroll);
                }
            };

        customScrollCheck();
        w.on('resize', customScrollCheck);
    }
})(jQuery);
