<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#fff">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keyword')">

    {{--<!-- CSRF Token -->--}}
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}

    <title>@yield('meta_title')</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png') }}">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">
    <link rel="manifest" href="{{ asset('images/favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/libs.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
</head>

<body>
<div class="content-wrap out {{ isset($global_data['currentPage']) && $global_data['currentPage'] === '' ? 'home' : '' }}" id="app">
    <header class="header {{ isset($global_data['currentPage']) && $global_data['currentPage'] === '' ? 'header-home' : '' }}">
        <div class="menu-open"><span></span></div>
        <div class="container">
            <div class="row">
                <div class="col-md-1 col-12"><a href="/" class="header__logo"><img src="images/logo.png" alt=""></a></div>
                <div class="col-md-11 d-none d-md-block">
                    <nav class="header__nav">
                        <ul>
                            @foreach($topMenu->roots() as $topMenuItem)
                                <li class="nav-item {{ (URL::current() == $topMenuItem->url()) ? "active" : '' }}">
                                    <a class="nav-link" href="{{ $topMenuItem->url() }}">{{ $topMenuItem->title }}</a>
                                </li>
                            @endforeach

                            @auth
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('admin.dashboard') }}">Admin Panel</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endauth
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <div class="bg"></div>
    <div class="menu-mobile">
        <div class="menu-close"></div><a href="/" class="menu-mobile__logo"><img src="images/logo.png" alt=""></a>
        <ul>
            @foreach($topMenu->roots() as $topMenuItem)
                <li class="nav-item {{ (URL::current() == $topMenuItem->url()) ? "active" : '' }}">
                    <a class="nav-link" href="{{ $topMenuItem->url() }}">{{ $topMenuItem->title }}</a>
                </li>
            @endforeach

            @auth
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('admin.dashboard') }}">Admin Panel</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endauth
        </ul>
    </div>

    <main role="main" class="content">
        @yield('content')
    </main>

    <footer class="footer">
        <div class="row no-gutters">
            <div class="col-12">
                <div class="container">
                    <div class="row no-gutters footer__subfooter d-flex">
                        <div class="col-12 col-md-4 footer__address">
                            @if(isset($global_data['settings']['public_address']))
                                {!! nl2br($global_data['settings']['public_address']) !!}
                            @endif
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="footer__contacts">
                                <div class="footer__contacts-item">
                                    @if(isset($global_data['settings']['public_email']))
                                        <img src="{{ asset('images/footer-envelop.svg') }}" alt="Email">
                                        <span>{{ $global_data['settings']['public_email'] }}</span>
                                    @endif
                                </div>
                                <div class="footer__contacts-item">
                                    @if(isset($global_data['settings']['public_phone']))
                                        <img src="{{ asset('images/footer-phone.svg') }}" alt="Email">
                                        <span>{{ $global_data['settings']['public_phone'] }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 footer__menu">
                            <ul class="list-inline mb-0">
                                @foreach($bottomMenu->roots() as $bottomMenuItem)
                                    <li class="list-inline-item {{ (URL::current() == $bottomMenuItem->url()) ? "active" : '' }}">
                                        <a class="nav-link" href="{{ $bottomMenuItem->url() }}">{{ $bottomMenuItem->title }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="footer__copyright">
                    <div class="container text-center">
                        {{ __('© The Council on Middle East Relations ') . date('Y') }}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div> <!-- .content_wrap -->

<script src="{{ asset('js/app.min.js') }}" defer></script>
<script src="{{ asset('js/common.min.js') }}" defer></script>

</body>

</html>
