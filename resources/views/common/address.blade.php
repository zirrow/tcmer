<div class="membership__form__info">
    <p>Contact the Council to submit your membership application or to make your donation.</p><b>Contact the National Director at:</b>
    <p>
        @if(isset($global_data['settings']['public_address']))
            {!! nl2br($global_data['settings']['public_address']) !!}
        @endif
    </p>
    <div>
        <h3>Included in all memberships</h3>
        <ul>
            <li>Members Events</li>
            <li>Members' Newsletter </li>
            <li>TCMER reports and papers, expert comment and news</li>
            <li>TCMER online Archives</li>
            <li>30% discount on books </li>
            <li>Ask TCMER - submit questions on topics and themes you want more information about. If selected, our experts deliver the answers.</li>
            <li>Exclusive bi-monthly insights from our Director’s Office, sharing all the latest thinking and analysis with you on major research topics.</li>
        </ul>
    </div>
</div>
