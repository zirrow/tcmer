@extends('layouts.app')

@section('meta_title')
    {{ $page_data['meta_title'] }}
@endsection

@section('meta_description')
    {{ $page_data['meta_description'] }}
@endsection

@section('meta_keyword')
    {{ $page_data['meta_keyword'] }}
@endsection

@section('content')
    @if($page_data['title'])
        <div class="page">
            <div class="container">
                <div class="page__title">
                    <h1>{{ $page_data['title'] }}</h1>
                </div>
                {!! $page_data['description'] !!}
            </div>
        </div>
    @else
        {!! $page_data['description'] !!}
    @endif
@endsection
