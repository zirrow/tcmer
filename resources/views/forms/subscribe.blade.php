<form id="subscribe-form" action="{{ route('mail.subscribe') }}" method="post">
    {{ csrf_field() }}
    <div class="row align-items-center">
        <div class="col-lg-8 col-md-7 col-12">
            <div class="home-subscription__input">
                <label for="subscribe-email">{{ __('EMAIL') }}</label>
                <input type="email" name="email" id="subscribe-email">
                <span>{{ __('* indicates required field') }}</span>
            </div>
        </div>
        <div class="col-lg-4 col-md-5 col-12">
            <input type="submit" value="{{ __('Subscribe to newsletter') }}" class="btn btn-secondary">
        </div>
    </div>
</form>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        (function($){
            $('#subscribe-form').on('submit', function(e){
                e.preventDefault();

                var form = $(this);

                form.find('.text-danger').remove();

                $.ajax({
                        url: form.attr('action'),
                        type: form.attr('method'),
                        dataType: 'json',
                        data: form.serialize()
                    })
                    .done(function(){
                        form.find('.row').html('<div class="alert alert-success w-100 text-center">{{ __('Thank you for subscribing !') }}</div>');
                    })
                    .fail(function(xhr){

                        var json = xhr.responseJSON;

                        if(typeof json['errors'] !== 'undefined'){
                            var h = '';

                            Object.keys(json['errors']).forEach(function(key){
                                json['errors'][key].forEach(function(item){
                                    h += item + '<br>';
                                });
                            });

                            form.find('#subscribe-email').after('<div class="text-danger">'+h+'</div>');
                        }
                    });
            })
        })(jQuery);
    });
</script>