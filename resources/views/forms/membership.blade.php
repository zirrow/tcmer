
<form id="membership-form" class="form" action="{{ route('mail.membership') }}#membership-form" method="post">
    {{ csrf_field() }}
    @if (session('status'))
        @if(session('status') === 'done')
        <div class="alert alert-success membership-alert" role="alert">
            {{ __('Here must be some success message.........') }} &nbsp;&nbsp;&nbsp;
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @elseif (session('status') === 'error')
            <div class="alert alert-error membership-alert" role="alert">
                {{ __('There was an error in the work. Please try again later.') }} &nbsp;&nbsp;&nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    @endif
    <div class="row">
        <div class="col-12">
            <div class="form__radio">
                <span>{{ __('There are three membership options') }}:</span>
                <input type="radio" name="membership" id="membership-options-1" value="{{ __('Full Member') }}" checked>
                <label for="membership-options-1">{{ __('Full Member') }}</label>
                <input type="radio" name="membership" id="membership-options-2" value="{{ __('Associate Member') }}">
                <label for="membership-options-2">{{ __('Associate Member') }}</label>
                <input type="radio" name="membership" id="membership-options-3" value="{{ __('Fellow Member') }}">
                <label for="membership-options-3">{{ __('Fellow Member') }}</label>
                @if($errors->has('membership'))
                    <p class="text-danger">{{ $errors->first('membership') }}</p>
                @endif
            </div>
        </div>
        <div class="col-lg-8">
            <div class="form__input">
                <span>{{ __('Title') }}:</span>
                <input type="text" name="title" placeholder="{{ __('Title') }}" value="{{ old('title') }}"
                       class="form-control {{ $errors->has('title') ? "is-invalid" : "" }}">
                @if($errors->has('title'))
                    <p class="text-danger">{{ $errors->first('title') }}</p>
                @endif
            </div>
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="form__input">
                        <span>{{ __('First name') }}:</span>
                        <input type="text" name="firstname" placeholder="{{ __('First name') }}" value="{{ old('firstname') }}"
                               class="form-control {{ $errors->has('firstname') ? "is-invalid" : "" }}">
                        @if($errors->has('firstname'))
                            <p class="text-danger">{{ $errors->first('firstname') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form__input">
                        <span>{{ __('Surname') }}:</span>
                        <input type="text" name="surname" placeholder="{{ __('Surname') }}" value="{{ old('surname') }}"
                               class="form-control {{ $errors->has('surname') ? "is-invalid" : "" }}">
                        @if($errors->has('surname'))
                            <p class="text-danger">{{ $errors->first('surname') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form__input">
                <span>{{ __('Entity') }}:</span>
                <input type="text" name="entity" placeholder="{{ __('Entity') }}" value="{{ old('entity') }}"
                       class="form-control {{ $errors->has('entity') ? "is-invalid" : "" }}">
                @if($errors->has('entity'))
                    <p class="text-danger">{{ $errors->first('entity') }}</p>
                @endif
            </div>
            <div class="form__input">
                <span>{{ __('Address') }}:</span>
                <input type="text" name="address" placeholder="{{ __('Address') }}" value="{{ old('address') }}"
                       class="form-control {{ $errors->has('address') ? "is-invalid" : "" }}">
                @if($errors->has('address'))
                    <p class="text-danger">{{ $errors->first('address') }}</p>
                @endif
            </div>
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="form__input">
                        <span>{{ __('Phone') }}:</span>
                        <input type="tel" name="phone" placeholder="{{ __('Phone') }}" value="{{ old('phone') }}"
                               class="form-control {{ $errors->has('phone') ? "is-invalid" : "" }}">
                        @if($errors->has('phone'))
                            <p class="text-danger">{{ $errors->first('phone') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form__input">
                        <span>{{ __('Email') }}:</span>
                        <input type="email" name="email" placeholder="{{ __('Email') }}" value="{{ old('email') }}"
                               class="form-control {{ $errors->has('email') ? "is-invalid" : "" }}">
                        @if($errors->has('email'))
                            <p class="text-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form__input">
                <span>{{ __('Website') }}:</span>
                <input type="text" name="website" placeholder="{{ __('Website') }}" value="{{ old('website') }}"
                       class="form-control {{ $errors->has('website') ? "is-invalid" : "" }}">
                @if($errors->has('website'))
                    <p class="text-danger">{{ $errors->first('website') }}</p>
                @endif
            </div>
            <div class="form__textarea">
                <span>{{ __('All applicants are required to submit a short personal statement for review by the Council’s Director who approves all members.') }}</span>
                <textarea placeholder="Your Text" name="description"
                          class="form-control {{ $errors->has('description') ? "is-invalid" : "" }}">{{ old('description') }}</textarea>
                @if($errors->has('description'))
                    <p class="text-danger">{{ $errors->first('description') }}</p>
                @endif
            </div>
            <input type="submit" value="{{ __('Submit form') }}" class="btn btn-secondary">
        </div>
        <div class="col-lg-4">
            @include('common.address')
        </div>
    </div>
</form>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        (function($){
            var messageFlash = $('.membership-alert');
            if(messageFlash.length){
                setTimeout(function(){
                    messageFlash.fadeOut().remove();
                }, 8000);
            }
        })(jQuery);
    });
</script>