<h2>Received a new message through the Membership form !</h2>

<ul>
    <li><strong>Membership: </strong> {{ $data->membership }}</li>
    <li><strong>Title: </strong> {{ $data->title }}</li>
    <li><strong>First name: </strong> {{ $data->firstname }}</li>
    <li><strong>Surname: </strong> {{ $data->surname }}</li>
    <li><strong>Entity: </strong> {{ $data->entity }}</li>
    <li><strong>Address: </strong> {{ $data->address }}</li>
    <li><strong>Phone: </strong> {{ $data->phone }}</li>
    <li><strong>Email: </strong> {{ $data->email }}</li>
    <li><strong>Website: </strong> {{ $data->website }}</li>
    <li><strong>Description: </strong> {{ $data->description }}</li>
</ul>
