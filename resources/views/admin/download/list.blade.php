@extends('admin.layouts.main')


@section('content')
    <section>
        <div class="container">
            <div class="buttons float-right mb-2">
                <a href="{{ route('admin.download.reload') }}" class="btn btn-sm btn-success">{{ __('Refresh file list') }}</a>
            </div>
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th class="w-25">{{ __('Name') }}</th>
                    <th>{{ __('Download Url') }}</th>
                    <th class="w-25">{{ __('Path') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($docs as $doc)
                    <tr>
                        <td>{{ $doc['name'] }}</td>
                        <td>{{ route('download', $doc['alias']) }}</td>
                        <td>{{ $doc['path'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection

@section('scripts')

@endsection