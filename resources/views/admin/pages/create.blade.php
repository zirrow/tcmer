@extends('admin.layouts.main')

@section('content')
    <section>
        <div class="container">
            <h2>{{ __('Add new page') }}</h2>
            <hr>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('admin.pages.store') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group required">
                    <label for="pageTitle">{{ __('Title H1') }}</label>
                    <input type="text" class="form-control {{ $errors->first('title') ? "is-invalid" : "" }}"  value="{{ old('title') }}" name="title" id="pageTitle" placeholder="{{ __('Enter page title H1') }}">
                    @if($errors->first('title'))
                        <p class="text-danger">{{ $errors->first('title') }}</p>
                    @endif
                </div>
                <div class="form-group required">
                    <label for="pageAlias">{{ __('Alias') }}</label>
                    <input type="text" class="form-control {{ $errors->first('alias') ? "is-invalid" : "" }}"  value="{{ old('alias') }}" name="alias" id="pageAlias" placeholder="{{ __('Enter page alias') }}">
                    @if($errors->first('alias'))
                        <p class="text-danger">{{ $errors->first('alias') }}</p>
                    @endif
                    <small id="aliasHelpBlock" class="form-text text-muted">
                        {{ __('Alias used like page URL. Sample: Alias - help. Result URL - http://site.com/help') }}
                    </small>
                </div>
                {{--<div class="form-group">--}}
                    {{--<label for="pageParent">{{ __('Parent page') }}</label>--}}
                    {{--<select multiple class="form-control" name="parent_id[]" id="pageParent">--}}
                        {{--@foreach($pages as $page)--}}
                            {{--<option value="{{ $page->id }}" {{ true === in_array($page->id, (array)old('parent_id'), 0) ? "selected" : "" }}>{{ $page->title }}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="form-group">
                    <label for="pageMetaTitle">{{ __('Title') }}</label>
                    <input type="text" class="form-control"  value="{{ old('meta_title') }}" name="meta_title" id="pageMetaTitle" placeholder="{{ _('Enter page title') }}">
                </div>
                <div class="form-group">
                    <label for="pageMetaKeywords">{{ __('Meta keywords') }}</label>
                    <input type="text" class="form-control"  value="{{ old('meta_keyword') }}" name="meta_keyword" id="pageMetaKeywords" placeholder="{{ _('Enter page meta keywords') }}">
                </div>
                <div class="form-group">
                    <label for="pageMetaDescription">{{ __('Meta description') }}</label>
                    <input type="text" class="form-control"  value="{{ old('meta_description') }}" name="meta_description" id="pageMetaDescription" placeholder="{{ _('Enter page meta description') }}">
                </div>
                <div class="form-group {{ old('description_clone') > 0 ? "d-none" : "required" }} ">
                    <label for="pageContent">{{ __('Page content') }}</label>
                    <textarea class="form-control" name="description" id="pageContent" rows="3">{{ old('description') }}</textarea>
                    @if($errors->first('description'))
                        <p class="text-danger">{{ $errors->first('description') }}</p>
                    @endif
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" name="is_description_cloned" value="1" type="checkbox"
                               id="pageContentIsCloned" {{ old('description_clone') > 0 ? "checked" : "" }}>
                        <label class="form-check-label"
                               for="pageContentIsCloned">{{ __('Clone content from another page') }}</label>
                    </div>
                </div>
                <div class="form-group {{ old('description_clone') > 0 ? "required" : "d-none" }}">
                    <label for="pageContentClone">{{ __('Clone content from:') }}</label>
                    <select class="form-control" name="description_clone" id="pageContentClone">
                        <option value="0">{{ __('Select one') }}</option>
                        @foreach($content_pages as $page)
                            <option value="{{ $page->id }}" {{ (int)old('description_clone') === (int)$page->id ? "selected" : "" }}>{{ $page->title }}</option>
                        @endforeach
                    </select>
                    @if($errors->first('description_clone'))
                        <p class="text-danger">{{ $errors->first('description_clone') }}</p>
                    @endif
                </div>
                <div class="form-group">
                    <span class="text-bold">{{ __('Status: ') }}</span>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" name="status" type="radio" id="pageStatusOff" value="0" {{ old('status') == 0 ? "checked" : "" }}>
                        <label class="form-check-label" for="pageStatusOff">{{ __('Disabled') }}</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" name="status" type="radio" id="pageStatusOn" value="1" {{ old('status') == 1 ? "checked" : "" }}>
                        <label class="form-check-label" for="pageStatusOn">{{ __('Enabled') }}</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" name="top_menu" type="checkbox" value="1" id="pageTopMenu" {{ old('top_menu') > 0 ? "checked" : "" }}>
                        <label class="form-check-label" for="pageTopMenu">{{ __('Show page in Top Menu') }}</label>
                    </div>
                </div>
                <div class="form-group {{ old('top_menu') > 0 ? "" : "d-none" }}">
                    <label for="pageTopMenuOrder">{{ __('Top menu order') }}</label>
                    <input type="text" class="form-control"  value="{{ old('top_menu') }}" name="top_menu_order" id="pageTopMenuOrder" placeholder="{{ _('Enter top menu order') }}">
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" name="bottom_menu" type="checkbox" value="1" id="pageBottomMenu" {{ old('bottom_menu') > 0 ? "checked" : "" }}>
                        <label class="form-check-label" for="pageBottomMenu">{{ __('Show page in Bottom Menu') }}</label>
                    </div>
                </div>
                <div class="form-group {{ old('bottom_menu') > 0 ? "" : "d-none" }}">
                    <label for="pageBottomMenuOrder">{{ __('Bottom menu order') }}</label>
                    <input type="text" class="form-control"  value="{{ old('bottom_menu') }}" name="bottom_menu_order" id="pageBottomMenuOrder" placeholder="{{ _('Enter bottom menu order') }}">
                </div>
                <hr>
                <a href="{{ route('admin.pages.index') }}" class="btn btn-outline-danger">{{ __('Cancel') }}</a>
                <button type="submit" class="btn btn-outline-primary float-right">{{ __('Save') }}</button>
            </form>
        </div>
    </section>
@endsection

@section('styles')
    <link href="{{ asset('js/summernote/summernote-bs4.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('js/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        (function($){
            $(document).ready(function() {
                $('#pageContent').summernote({
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                        ['fontname', ['fontname']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['hr']],
                        ['view', ['fullscreen', 'codeview', 'help']],
                    ],
                    tabsize: 2,
                    height: 400
                });
            });

            $('#pageTopMenu, #pageBottomMenu').on('change', function(){
                var orderField = '#' + $(this).attr('id') + 'Order';
                if($(this).is(":checked")) {
                    $(orderField).closest('.form-group').removeClass('d-none');
                } else {
                    $(orderField).closest('.form-group').addClass('d-none');
                }
                $(orderField).val(0);
            });

            $('#pageContentIsCloned').on('change', function(){
                var description = $('#pageContent'),
                    cloned = $('#pageContentClone');
                if($(this).is(":checked")) {
                    description.closest('.form-group').removeClass('required').addClass('d-none');
                    description.summernote('disable');
                    cloned.closest('.form-group').removeClass('d-none').addClass('required');
                } else {
                    description.closest('.form-group').removeClass('d-none').addClass('required');
                    description.summernote('enable');
                    cloned.closest('.form-group').removeClass('required').addClass('d-none');
                }
                cloned.val(0);
                description.val('');
            });
        })(jQuery);
    </script>
@endsection