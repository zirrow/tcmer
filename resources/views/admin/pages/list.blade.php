@extends('admin.layouts.main')


@section('content')
    <section>
        <div class="container">
            <div class="buttons float-right mb-2">
                <a href="{{ route('admin.pages.create') }}" class="btn btn-sm btn-success">{{ __('Add page') }}</a>
            </div>
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th class="w-50">{{ __('Title') }}</th>
                    <th class="w-25">{{ __('Status') }}</th>
                    <th class="w-25 text-center">{{ __('Actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                    <tr>
                        <td>{{ $page->title }}</td>
                        <td>{{ $page->status ? "Enabled" : "Disabled" }}</td>
                        <td class="text-center">
                            <a href="{{ route('pages', $page->alias) }}" class="btn btn-sm btn-info"
                               title="{{ __('View page') }}">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{ route('admin.pages.edit', $page->id) }}" class="btn btn-sm btn-warning"
                               title="{{ __('Edit') }}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <button type="submit" form="page-delete-{{ $page->id }}" class="btn btn-sm btn-danger"
                                    title="{{ __('Delete') }}">
                                <i class="fa fa-times"></i>
                            </button>
                            <form action="{{ route('admin.pages.destroy', $page->id) }}" method="POST"
                                  id="page-delete-{{ $page->id }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection