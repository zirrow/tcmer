@extends('admin.layouts.main')


@section('content')
    <section>
        <div class="container">
            <div class="buttons float-right mb-2">
                <a href="{{ route('admin.settings.create') }}" class="btn btn-sm btn-success">{{ __('Add new') }}</a>
            </div>
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th class="w-50">{{ __('Key') }}</th>
                    <th class="w-25">{{ __('Value') }}</th>
                    <th class="w-25 text-center">{{ __('Actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($settings as $setting)
                    <tr>
                        <td>{{ $setting['key'] }}</td>
                        <td>
                            {{ $setting['value'] }}
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.settings.edit', $setting['id']) }}" class="btn btn-sm btn-warning"
                               title="{{ __('Edit') }}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <button type="submit" form="setting-delete-{{ $setting['id'] }}" class="btn btn-sm btn-danger"
                                    title="{{ __('Delete') }}">
                                <i class="fa fa-times"></i>
                            </button>
                            <form action="{{ route('admin.settings.destroy', $setting['id']) }}" method="POST"
                                  id="setting-delete-{{ $setting['id'] }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection