@extends('admin.layouts.main')

@section('content')
    <section>
        <div class="container">
            <h2>{{ __('Edit setting') }}</h2>
            <hr>
            <form action="{{ route('admin.settings.update', $current->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group required">
                    <label for="settingKey">{{ __('Key name') }}</label>
                    <input type="text" class="form-control {{ $errors->first('key') ? "is-invalid" : "" }}"
                           value="{{ $current->key }}" name="key" id="settingKey"
                           placeholder="{{ __('Enter key name') }}">
                    @if($errors->first('key'))
                        <p class="text-danger">{{ $errors->first('key') }}</p>
                    @endif
                </div>
                <div class="form-group required">
                    <label for="settingValue">{{ __('Value') }}</label>
                    <textarea type="text" class="form-control {{ $errors->first('value') ? "is-invalid" : "" }}"
                              rows="4" name="value" id="settingValue"
                              placeholder="{{ __('Enter value') }}">{{ $current->value }}</textarea>
                    @if($errors->first('value'))
                        <p class="text-danger">{{ $errors->first('value') }}</p>
                    @endif
                </div>
                <hr>
                <a href="{{ route('admin.settings.index') }}" class="btn btn-outline-danger">{{ __('Cancel') }}</a>
                <button type="submit" class="btn btn-outline-primary float-right">{{ __('Save') }}</button>
            </form>
        </div>
    </section>
@endsection