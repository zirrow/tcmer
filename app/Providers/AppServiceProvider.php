<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
	    //global data
	    view()->composer('*', function($view){

	        // settings
		    $settings = Cache::remember('settings', 3600, function(){

                $data = [];

                $settings = Setting::all();

                foreach ($settings as $setting){
                    $data[$setting->key] = $setting->value;
                }

                return $data;
            });

		    //current page
            $currentPage = '';

            $route = Route::getFacadeRoot()->current();

            if(true === method_exists($route,  'uri') && $route->uri() === '{page?}'){
                if (method_exists($route, 'parameters')) {

                    $parameters = $route->parameters();

                    if (true === array_key_exists('page', $parameters)) {
                        $currentPage = $parameters['page'];
                    }
                }
            }

		    $view->with('global_data', collect([
			    'settings' => $settings,
                'currentPage' => $currentPage
		    ]));
	    });

	    //IDE Helper
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
