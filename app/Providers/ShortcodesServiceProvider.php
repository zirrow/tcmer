<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Gornymedia\Shortcodes\Facades\Shortcode;

class ShortcodesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * @usage [forms name="contacts"] - appended file resource/views/forms/contacts.blade.php
         */
        Shortcode::add('forms', function ($atts, $content, $name) {

            $data = [
                'name' => $name
            ];

            $a = Shortcode::atts($data, $atts);

            $file = 'forms/' . $a['name']; // ex: resource/views/forms/ $atts['name'] .blade.php

            if (view()->exists($file)) {
                return view($file, $a);
            }

            return null;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
