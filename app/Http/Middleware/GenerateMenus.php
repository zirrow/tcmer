<?php

namespace App\Http\Middleware;

use App\Models\Page;
use Illuminate\Support\Facades\Cache;
use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

	public function handle($request, Closure $next)
	{
		\Menu::make('topMenu', function ($menu) {

            $pages = Cache::remember('top_menu', 3600, function(){

                return Page::where('top_menu', '>', 0)->where('status', 1)->orderBy('top_menu', 'asc')->get();
            });

			foreach ($pages as $page){

				if(empty($page->alias) || $page->alias === 'home'){
					$page->alias = '';
				}

				$menu->add($page->title, (string)$page->alias)->id($page->id);
			}
		});

		\Menu::make('bottomMenu', function ($menu) {

            $pages = Cache::remember('bottom_menu', 3600, function(){

                return Page::where('bottom_menu', '>', 0)->where('status', 1)->orderBy('bottom_menu', 'asc')->get();
            });

			foreach ($pages as $page){
				if(empty($page->alias) || $page->alias === 'home'){
					$page->alias = '';
				}

				$menu->add($page->title, ['url' => (string) $page->alias])->id($page->id);

			}
		});

		return $next($request);
	}
}
