<?php

namespace App\Http\Controllers\Mail;

use App\Http\Controllers\Controller;
use App\Mail\SubscribeEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SubscribeController extends Controller
{
    public function index(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'email'       => 'required',
        ]);

        if ($validator->fails()) {
            return bad_request([
                'status' => 'error',
                'message' => 'validation error',
                'errors' => $validator->errors()
            ]);
        }

        // prepare email data
        $objMsg = new \stdClass();

        $objMsg->email = $request->input('email');

        //send email
        try {
            Mail::send(new SubscribeEmail($objMsg));

            return ok([
                'status' => 'done'
            ]);

        } catch (\Throwable $e){

            Log::error($e->getMessage() . PHP_EOL . $e->getTraceAsString());

            return bad_request([
                'status' => 'error',
                'message' => 'error sending email',
                'errors' => [
                    'send' => [
                        'There was an error in the work. Please try again later.'
                    ]
                ]
            ]);
        }
    }
}