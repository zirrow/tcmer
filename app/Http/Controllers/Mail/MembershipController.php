<?php

namespace App\Http\Controllers\Mail;

use App\Http\Controllers\Controller;
use App\Mail\MembershipEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class MembershipController extends Controller
{
    public function index(Request $request)
    {
        //validation
        $request->validate([
            'membership'  => 'required',
            'title'       => 'required|min:2|max:254',
            'firstname'   => 'required|min:2|max:254',
            'surname'     => 'required|min:2|max:254',
            'entity'      => 'required',
            'address'     => 'required',
            'phone'       => 'required',
            'email'       => 'required',
            'website'     => 'required',
            'description' => 'required',
        ]);

        // prepare email data
        $objMsg = new \stdClass();

        $objMsg->membership = $request->input('membership');
        $objMsg->title = $request->input('title');
        $objMsg->firstname = $request->input('firstname');
        $objMsg->surname = $request->input('surname');
        $objMsg->entity = $request->input('entity');
        $objMsg->address = $request->input('address');
        $objMsg->phone = $request->input('phone');
        $objMsg->email = $request->input('email');
        $objMsg->website = $request->input('website');
        $objMsg->description = \strip_tags($request->input('description'));

        //send email
        try {
            Mail::send(new MembershipEmail($objMsg));

            return response()->redirectTo('/membership')->with('status', 'done');

        } catch (\Throwable $e){

            Log::error($e->getMessage() . PHP_EOL . $e->getTraceAsString());

            return response()->redirectTo('/membership')->with('status', 'error');
        }
    }
}