<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('download');
    }
    
    
    public function index()
    {
        $docs = $this->allAvailableDocs();

        return view('admin.download.list', compact('docs'));
    }

    public function reload()
    {
        Cache::forget('docs');

        return response()->redirectToRoute('admin.download.index');
    }
    
    public function download($alias)
    {
        if($doc = $this->checkAlias($alias)){

            $headers = [
                'Content-Type' => $doc['type'],
            ];

            return response()->download(Storage::path($doc['path']), $doc['name'], $headers);
        }

        return abort(404);
    }

    private function checkAlias($alias)
    {
        $needed_doc = false;

        $docs = $this->allAvailableDocs();

        foreach ($docs as $doc) {

            if($alias === $doc['alias']){

                $needed_doc = $doc;

                break;
            }
        }

        return $needed_doc;
    }

    private function allAvailableDocs()
    {

        return Cache::remember('docs', 3600, function(){

            $docs = [];

            $files = Storage::allFiles('public/docs');

            $mimes = [
                'pdf'  => 'application/pdf',
                'jpg'  => 'image/jpeg',
                'jpeg' => 'image/jpeg',
            ];

            foreach ($files as $file) {

                $fileinfo = pathinfo($file);

                $docs[] = [
                    'type'  => true === array_key_exists($fileinfo['extension'], $mimes)
                        ? $mimes[$fileinfo['extension']]
                        : 'plain/text',
                    'alias' => $this->stringToAlias($fileinfo['basename']),
                    'name'  => $fileinfo['basename'],
                    'path'  => $file
                ];
            }

            return $docs;
        });
    }

    private function stringToAlias($string)
    {
        $translate_array = [
            //Rus
            'А'=>'a','Б'=>'b','В'=>'v','Г'=>'g',
            'Д'=>'d','Е'=>'e','Ё'=>'e','Ж'=>'j','З'=>'z',
            'И'=>'i','Й'=>'y','К'=>'k','Л'=>'l',
            'М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r',
            'С'=>'s','Т'=>'t','У'=>'u','Ф'=>'f','Х'=>'h',
            'Ц'=>'ts','Ч'=>'ch','Ш'=>'sh','Щ'=>'sch','Ъ'=>'',
            'Ы'=>'yi','Ь'=>'','Э'=>'e','Ю'=>'yu','Я'=>'ya',
            'а'=>'a','б'=>'b','в'=>'v','г'=>'g',
            'д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z',
            'и'=>'i','й'=>'y','к'=>'k','л'=>'l',
            'м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r',
            'с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h',
            'ц'=>'ts','ч'=>'ch','ш'=>'sh','щ'=>'sch','ъ'=>'y',
            'ы'=>'yi','ь'=>'','э'=>'e','ю'=>'yu','я'=>'ya',
            //Lat
            'A'=>'a','B'=>'b','C'=>'c','D'=>'d','E'=>'e',
            'F'=>'f','G'=>'g','H'=>'h','I'=>'i','J'=>'j',
            'K'=>'k','L'=>'l','M'=>'m','N'=>'n','O'=>'o',
            'P'=>'p','Q'=>'q','R'=>'r','S'=>'s','T'=>'t',
            'U'=>'u','V'=>'v','W'=>'w','X'=>'x','Y'=>'y','Z'=>'z',
            //Ukr
            'Є'=>'e','Ґ'=>'h','Ї'=>'i',
            'є'=>'e','ґ'=>'h','ї'=>'i',
            //Other_remove
            ',' => '', '`' => '', ':' => '', ';' => '',
            '№' => '', '=' => '', '>' => '', '<' => '', '|' => '',
            '°' => '', "'" => '', '"' => '', '&' => '',
            //Other_replace
            '(' => '-', ')' => '-', '*' => '-',
            '?' => '-', '!' => '-', '+' => '-', '/'=>'-','\\'=>'-',
            '  ' => '-',' ' => '-', '%' => '-', '_' => '-', '±' => '-',
        ];

        return preg_replace('/-$/','', preg_replace('/-+/','-', html_entity_decode( strtr($string, $translate_array))));
    }
}
