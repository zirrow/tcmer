<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Cache;

class PagesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $this->validate($request, [
            'name' => 'string|max:255|nullable',
            'page' => 'integer',
        ]);

        $name = null;
        if ($request->input('name') && !empty($request->input('name'))) {
            $data['name'] = $name = $request->input('name');
        }

        $data['pages'] = Page::when($name !== null, function ($query) use ($name) {
            $query->where('name', $name);
        })
            ->orderBy('id', 'desc')
            ->paginate(50);

        return view('admin.pages.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = Page::all();

        $content_pages = Page::where('description_clone', 0)->get();

        return view('admin.pages.create', compact('pages', 'content_pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //default values
        if (!$request->has('status')) {
            $request->merge(['status' => 0]);
        }
        if (!$request->has('parent_id')) {
            $request->merge(['parent_id' => 0]);
        }
        if (!$request->has('top_menu')) {
            $request->merge(['top_menu' => 0]);
        }
        if (!$request->has('bottom_menu')) {
            $request->merge(['bottom_menu' => 0]);
        }
        if (!$request->has('is_description_cloned')) {
            $request->merge(['is_description_cloned' => 0]);
        }
        if (!$request->has('description_clone')) {
            $request->merge(['description_clone' => 0]);
        }
        if ($request->has('top_menu_order') && !empty($request->input('top_menu_order'))) {
            $request->merge(['top_menu' => (int)$request->input('top_menu_order')]);
        }
        if ($request->has('bottom_menu_order') && !empty($request->input('bottom_menu_order'))) {
            $request->merge(['bottom_menu' => (int)$request->input('bottom_menu_order')]);
        }

        //validation
        $request->validate([
            'title'       => 'required|unique:pages,title|min:4|max:254',
            'alias'       => 'required|unique:pages,alias|min:2|max:254',
            'description' => 'required_if:is_description_cloned,0',
            'description_clone' => 'required_if:is_description_cloned,1',
        ]);

        //store
        Page::create([
            'alias'            => $request->input('alias'),
            'parent_id'        => $request->input('parent_id'),
            'title'            => $request->input('title'),
            'description'      => htmlentities($request->input('description')),
            'description_clone'  => $request->input('description_clone'),
            'meta_title'       => $request->input('meta_title'),
            'meta_description' => $request->input('meta_description'),
            'meta_keyword'     => $request->input('meta_keyword'),
            'status'           => (int)$request->input('status'),
            'top_menu'         => (int)$request->input('top_menu'),
            'bottom_menu'      => (int)$request->input('bottom_menu'),
        ]);

        Cache::forget('top_menu');
        Cache::forget('bottom_menu');

        return response()->redirectToRoute('admin.pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $current = Page::findOrFail($id);

        return response()->redirectTo($current->alias);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $current = Page::findOrFail($id);

        $pages = Page::where('id', '!=', $current->id)->get();

        $content_pages = Page::where('description_clone', 0)->where('id', '!=', $current->id)->get();

        $current->description = html_entity_decode($current->description, ENT_QUOTES, 'UTF-8');

        return view('admin.pages.edit', compact('pages', 'content_pages', 'current'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //default values
        if (!$request->has('status')) {
            $request->merge(['status' => 0]);
        }
        if (!$request->has('parent_id')) {
            $request->merge(['parent_id' => 0]);
        }
        if (!$request->has('top_menu')) {
            $request->merge(['top_menu' => 0]);
        }
        if (!$request->has('bottom_menu')) {
            $request->merge(['bottom_menu' => 0]);
        }
        if (!$request->has('is_description_cloned')) {
            $request->merge(['is_description_cloned' => 0]);
        }
        if (!$request->has('description_clone')) {
            $request->merge(['description_clone' => 0]);
        }
        if ($request->has('top_menu_order') && !empty($request->input('top_menu_order'))) {
            $request->merge(['top_menu' => (int)$request->input('top_menu_order')]);
        }
        if ($request->has('bottom_menu_order') && !empty($request->input('bottom_menu_order'))) {
            $request->merge(['bottom_menu' => (int)$request->input('bottom_menu_order')]);
        }

        //validation
        $request->validate([
            'title'       => ['required', Rule::unique('pages', 'title')->ignore($id), 'min:4', 'max:254'],
            'alias'       => ['required', Rule::unique('pages', 'alias')->ignore($id), 'min:2', 'max:254'],
            'description' => 'required_if:is_description_cloned,0',
            'description_clone' => 'required_if:is_description_cloned,1',
        ]);
        
        $current = Page::findOrFail($id);

        //update
        $current->alias = $request->input('alias');
        $current->parent_id = $request->input('parent_id');
        $current->title = $request->input('title');
        $current->description = htmlentities($request->input('description'));
        $current->description_clone = (int) $request->input('description_clone');
        $current->meta_title = $request->input('meta_title');
        $current->meta_description = $request->input('meta_description');
        $current->meta_keyword = $request->input('meta_keyword');
        $current->status = (int)$request->input('status');
        $current->top_menu = (int)$request->input('top_menu');
        $current->bottom_menu = (int)$request->input('bottom_menu');

        $current->save();

        Cache::forget('top_menu');
        Cache::forget('bottom_menu');

        return response()->redirectToRoute('admin.pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $current = Page::findOrFail($id);

        $current->delete();

        Cache::forget('top_menu');
        Cache::forget('bottom_menu');

        return response()->redirectToRoute('admin.pages.index');
    }
}
