<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Cache;

class SettingsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $this->validate($request, [
            'name' => 'string|max:255|nullable',
            'page' => 'integer',
        ]);

        $name = null;
        if ($request->input('name') && !empty($request->input('name'))) {
            $name = $request->input('name');
        }

        $settings = Setting::when($name !== null, function ($query) use ($name) {
            $query->where('name', $name);
        })
            ->orderBy('id', 'desc')
            ->paginate(50);

        $data = [
            'settings' => []
        ];

        foreach ($settings as $setting) {
            $data['settings'][] = [
                'id'   => $setting['id'],
                'key'   => $setting['key'],
                'value' => \stringFunction::limitText($setting['value'], 40),
            ];
        }

        return view('admin.settings.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'key'   => 'required|unique:settings,key|min:2|max:254',
            'value' => 'required'
        ]);

        //store
        Setting::create([
            'key'   => $request->input('key'),
            'value' => $request->input('value')
        ]);

        Cache::forget('settings');

        return response()->redirectToRoute('admin.settings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->redirectToRoute('admin.settings.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $current = Setting::findOrFail($id);

        return view('admin.settings.edit', compact('current'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'key'   => ['required', Rule::unique('settings', 'key')->ignore($id), 'min:2', 'max:254'],
            'value' => 'required'
        ]);


        $current = Setting::findOrFail($id);

        //update
        $current->key = $request->input('key');
        $current->value = $request->input('value');

        $current->save();

        Cache::forget('settings');

        return response()->redirectToRoute('admin.settings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $current = Setting::findOrFail($id);

        $current->delete();

        Cache::forget('settings');

        return response()->redirectToRoute('admin.settings.index');
    }
}
