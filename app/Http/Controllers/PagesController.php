<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Show the application.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $page = null)
    {
        $alias = 'home';

        if(null !== $page){
            $alias = $page;
        }

	    return $this->build($alias);
    }

    /**
     * Build the page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    private function build($alias)
    {
        $page = Page::where('alias', (string) $alias)->where('status', '1')->first();


        if ($page) {

            $is_hidden_title = false;
            if($page->alias === 'home'){
                $is_hidden_title = true;
            }

            $content = $page->description;

            if($page->description_clone){
                $content_source = Page::find($page->description_clone);

                if($content_source){
                    $content = $content_source->description;
                }
            }

            $page_data = [
                'id'               => $page->id,
                'alias'            => $page->alias,
                'parent_id'        => $page->parent_id,
                'title'            => $is_hidden_title ? null : $page->title,
                'description'      => html_entity_decode($content, ENT_QUOTES, 'UTF-8'),
                'meta_title'       => $page->meta_title,
                'meta_description' => $page->meta_description,
                'meta_keyword'     => $page->meta_keyword,
                'form_data'        => $page->form_data,
                'created_at'       => $page->created_at,
            ];

            return view('common.page', ['page_data' => $page_data])->compileShortcodes();

        } else {

            return abort(404);
        }
    }
}
