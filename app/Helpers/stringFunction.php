<?php

/**
 * Class stringFunction
 */
class stringFunction
{
    /**
     * remove all text exept limited count simbols
     * @param $text
     * @param int $limit
     * @return string
     */
    public static function limitText($text, $limit = 100)
    {
        $text = \strip_tags(\html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
        $out = $text;
        if (\utf8_strlen($text) > $limit) {
            $out = \utf8_substr($text, 0, $limit) . '...';
        }
        return $out;
    }


    /**
     * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
     * param  $number Integer Число на основе которого нужно сформировать окончание
     * param  $endingsArray  Array Массив слов или окончаний для чисел (1, 4, 5),
     *         например array('яблоко', 'яблока', 'яблок')
     * return String
     */
    public static function getNumEnding($number, array $endingArray)
    {
        $number = $number % 100;
        if ($number >= 11 && $number <= 19) {
            $ending = $endingArray[2];
        } else {
            $i = $number % 10;
            switch ($i) {
                case (1):
                    $ending = $endingArray[0];
                    break;
                case (2):
                case (3):
                case (4):
                    $ending = $endingArray[1];
                    break;
                default:
                    $ending = $endingArray[2];
            }
        }
        return $ending;
    }

}
