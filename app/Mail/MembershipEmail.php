<?php

namespace App\Mail;

use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MembershipEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The data object instance.
     *
     * @var
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sender = Setting::where('key', 'email_sender')->first()->value;
        $recepient = Setting::where('key', 'email_recepient')->first()->value;

        return $this->from($sender)
            ->to($recepient)
            ->subject(__('Site Membership Form'))
            ->view('mails.membership')
            ->text('mails.membership_plain');
    }
}
