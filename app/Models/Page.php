<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	protected $fillable = [
		'alias',
		'parent_id',
		'title',
		'description',
		'description_clone',
		'meta_title',
		'meta_description',
		'meta_keyword',
		'status',
		'top_menu',
		'main_menu',
		'bottom_menu',
	];

	protected $casts = [
		'form_data' => 'json',
        'parent_id' => 'array',
	];
}
